# handy-shell-scripts

A Project to house handy shell scripts for Linux (Contact me to contribute)

## Name
Handy Shell Scripts

## Description
This project will be used to house shell scripts for bash made by people around the world for convenience of automating default tasks

## Example
instead of" g++ -o main.out main.cpp -Wpedantic -O3"  
run "./opg++ main.cpp"

or

instead of "nasm -felf64 $FILE.asm && ld $FILE.o && ./a.out | cat > output.txt && cat output.txt && echo && rm $FILE.o a.out"  
run "./nasm64.sh <filename>.<extension-name>"

even for git:  
instead of git add . && git commit -m "<message>" && git push  

just do: "git-push message"

Many more such scripts are possible to simplify our daily tasks. This repository will house such scripts so that our peers do not need to re-write these scripts :)