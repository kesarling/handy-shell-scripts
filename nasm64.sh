if [[ -z $1 ]];
then
	echo "No input file specified"
	exit
elif [[ ! -f $1 ]];
then
	echo "File does not exist"
	exit
fi
FILE=`echo "$1" | cut -d'.' -f1`
nasm -felf64 $FILE.asm && ld $FILE.o && ./a.out | cat > output.txt && cat output.txt && echo && rm $FILE.o a.out
